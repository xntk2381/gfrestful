package gfrestful

import (
	"net/http"

	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/net/ghttp"
)

func Insert(r *ghttp.Request) {
	model := r.Get("model").String()
	if model == "" {
		r.Response.WriteHeader(http.StatusNotFound)
	}
	data, err := r.GetJson()
	if err != nil {
		g.Log().Error(r.Context(), err)
		r.Response.WriteHeader(http.StatusInternalServerError)
		r.Response.WriteExit(err)
	}
	_, err = g.DB().Ctx(r.Context()).Model(model).Insert(data.Map())
	if err != nil {
		g.Log().Error(r.Context(), err)
		r.Response.WriteHeader(http.StatusInternalServerError)
		r.Response.WriteExit(err)
	}
	r.Response.WriteHeader(http.StatusCreated)
}

func Delete(r *ghttp.Request) {
	model := r.Get("model").String()
	if model == "" {
		r.Response.WriteHeader(http.StatusNotFound)
	}
	id := r.Get("id")
	_, err := g.DB().Ctx(r.Context()).Model(model).Delete("id", id)
	if err != nil {
		g.Log().Error(r.Context(), err)
		r.Response.WriteHeader(http.StatusInternalServerError)
		r.Response.WriteExit(err)
	}
	r.Response.WriteHeader(http.StatusNoContent)
}

func Update(r *ghttp.Request) {
	model := r.Get("model").String()
	if model == "" {
		r.Response.WriteHeader(http.StatusNotFound)
	}
	id := r.Get("id")
	data, err := r.GetJson()
	if err != nil {
		g.Log().Error(r.Context(), err)
		r.Response.WriteHeader(http.StatusInternalServerError)
		r.Response.WriteExit(err)
	}
	_, err = g.DB().Ctx(r.Context()).Model(model).Update(data.Map(), "id=?", id)
	if err != nil {
		g.Log().Error(r.Context(), err)
		r.Response.WriteHeader(http.StatusInternalServerError)
		r.Response.WriteExit(err)
	}
	r.Response.WriteHeader(http.StatusOK)
}

// 通过ID查询
func SelectById(r *ghttp.Request) {
	model := r.Get("model").String()
	id := r.Get("id")
	re, err := g.DB().Ctx(r.Context()).Model(model).Where("id = ?", id).One()
	if err != nil {
		g.Log().Error(r.Context(), err)
		r.Response.WriteHeader(http.StatusInternalServerError)
		r.Response.WriteExit(err)
	}
	r.Response.WriteJsonExit(re.Json())
}

// 分页查
func Select(r *ghttp.Request) {

	model := r.Get("model").String()
	if model == "" {
		r.Response.WriteHeader(http.StatusNotFound)
	}
	m := g.DB().Ctx(r.Context()).Model(model)
	total, err := m.Count()
	if err != nil {
		g.Log().Error(r.Context(), err)
		r.Response.WriteHeader(http.StatusInternalServerError)
		r.Response.WriteExit(err)
	}
	fields := r.Get("fields").String()
	page := r.Get("page").Int()
	if page == 0 {
		page = 1
	}
	size := r.Get("size").Int()
	if size == 0 {
		size = 10
	}
	startNum := size * (page - 1)
	totalPage := total / size
	if total%size > 0 {
		totalPage++
	}
	orderby := r.Get("orderby").String()
	if orderby == "" {
		orderby = "id"
	}
	rs, err := m.Order(orderby).Fields(fields).Limit(startNum, size).All()
	if err != nil {
		g.Log().Error(r.Context(), err)
		r.Response.WriteHeader(http.StatusInternalServerError)
		r.Response.WriteExit(err)
	}
	r.Response.WriteJsonExit(g.Map{
		"total":     total,
		"list":      rs.List(),
		"totalPage": totalPage,
		"page":      page,
		"size":      size,
	})
}
