基于GoFrame框架开发的RESTFul api，实现了日志记录，sql语句打印，链路追踪。实现了数据的增、删、改、查（分页）。

用法 修改config.toml.example 为 config.toml，配置数据库信息，监听端口

启动服务 go run cmd/server/main.go

cmd/client/main.go 中演示了增删改查（分页）的 各种用法