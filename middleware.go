package gfrestful

import (
	"context"
	"time"

	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/net/ghttp"
)

func logmiddleware(r *ghttp.Request) {
	t := time.Now()
	ctx := r.Context()
	r.Middleware.Next()
	// 异步日志
	go func(ctx context.Context, t time.Time) {
		// g.Log().Debug(ctx, "Method", r.Method)
		// g.Log().Debug(ctx, "RequestURI", r.RequestURI)
		// if r.GetBodyString() != "" {
		// 	g.Log().Debug(ctx, "body", r.GetBodyString())
		// }
		// g.Log().Debug(ctx, "request", r.GetRequestMap())
		g.Log().Debug(ctx, r.Method, r.RequestURI, r.GetBodyString())
		g.Log().Debugf(ctx, "用时：%v", time.Since(t))
	}(ctx, t)
}
