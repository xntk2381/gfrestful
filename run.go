package gfrestful

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/net/ghttp"
)

func Run() {
	s := g.Server()
	g.DB().SetLogger(g.Log())
	s.Group("restful", func(group *ghttp.RouterGroup) {
		// 日志中间件
		group.Middleware(logmiddleware)
		// 增
		group.POST("/:model", Insert)
		// 删
		group.DELETE("/:model/:id", Delete)
		// 改
		group.PUT("/:model/:id", Update)
		// 分页查询
		group.GET("/:model", Select)
		// ID单查
		group.GET("/:model/:id", SelectById)
	})
	s.Run()
}
